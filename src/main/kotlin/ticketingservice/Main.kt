package ticketingservice

import com.sun.net.httpserver.HttpServer
import ticketingservice.handler.BookingHandler
import ticketingservice.respository.implementations.*
import ticketingservice.service.implementation.SeatServiceImpl
import ticketingservice.util.DataInitializer
import java.net.InetSocketAddress

fun main() {
    //Create data
    val stationRepository = StationRepositoryImpl()
    val routeRepository = RouteRepositoryImpl()
    val serviceRepository = ServiceRepositoryImpl()
    val carriageRepository = CarriageRepositoryImpl()
    val seatRepository = SeatRepositoryImpl()
    val seatServiceImpl = SeatServiceImpl(
        seatRepository,
        carriageRepository
    )
    val passengerRepository = PassengerRepositoryImpl()
    val stopRepository = StopRepositoryImpl()

    val dataInitializer = DataInitializer(
        stationRepository,
        routeRepository,
        serviceRepository,
        carriageRepository,
        seatServiceImpl,
        passengerRepository,
        stopRepository
    )

    dataInitializer.initializeData()

    //Initiate server
    val server = HttpServer.create(InetSocketAddress(7000), 0)
    server.createContext("/api/bookings", BookingHandler())
    server.start()


}