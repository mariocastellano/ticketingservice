package ticketingservice.controller

import ticketingservice.request.BookingRequest
import ticketingservice.response.BookingResponse
import ticketingservice.response.BookingResult
import ticketingservice.exceptions.SeatAlreadyTakenException
import ticketingservice.service.interfaces.BookingServiceInterface

class BookingController(private val bookingService: BookingServiceInterface) {

    fun createBooking(bookingRequest: BookingRequest): BookingResult {
        try {
            bookingService.isBookingAvailable(bookingRequest)
            val booking = bookingService.createBooking(bookingRequest)
            return BookingResult.Success(BookingResponse(booking.bookingId, booking.passengerName, booking.tickets))
        } catch (ex: SeatAlreadyTakenException) {
            return BookingResult.Error("Error: ${ex.message}")
        } catch (ex: Exception) {
            return BookingResult.Error("An unexpected error occurred")
        }
    }
}
