package ticketingservice.entity

data class Booking(
    val id: Int? = null,
    val passengerName: String,
    val tickets: List<Ticket>
)