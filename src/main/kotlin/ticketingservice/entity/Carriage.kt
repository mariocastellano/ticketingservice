package ticketingservice.entity

// I saw too late that the Carriage should have a name and seat should be CarriageName + Seat Name
data class Carriage(
    val id: Int? = null,
    val serviceId: Int? = null,
    val seats: List<Seat>? = null
)
