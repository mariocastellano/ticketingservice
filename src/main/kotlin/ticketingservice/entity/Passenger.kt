package ticketingservice.entity

data class Passenger(
    val id: Int? = null,
    val name: String,
    val bookings: List<Booking>? = null
)