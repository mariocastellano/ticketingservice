package ticketingservice.entity

data class Route(
    val id: Int? = null,
    val name: String,
    val stops: List<Stop>
)