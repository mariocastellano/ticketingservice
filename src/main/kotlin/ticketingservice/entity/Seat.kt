package ticketingservice.entity

import ticketingservice.enum.SeatType

data class Seat(
    val id: Int? = null,
    val seatNumber: String,
    val carriage: Carriage,
    val type: SeatType,
    val available: Boolean = true
)
