package ticketingservice.entity

import java.time.LocalDateTime

data class Service(
    val id: Int? = null,
    val route: Route,
    val departureTime: LocalDateTime,
    val carriages: List<Carriage>
)
