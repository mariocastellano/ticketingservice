package ticketingservice.entity

data class Station(
    val id: Int? = null,
    val name: String
)