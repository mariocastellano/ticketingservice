package ticketingservice.entity

class Stop(
    val id: Int? = null,
    val route: Route,
    val station: Station,
    val distanceFromPreviousStop: Int
)