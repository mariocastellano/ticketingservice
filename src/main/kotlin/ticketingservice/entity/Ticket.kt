package ticketingservice.entity

data class Ticket(
    val id: Int? = null,
    var bookingId: Int? = null,
    val serviceId: Int,
    val seatId: Int,
    val passengerId: Int
)
