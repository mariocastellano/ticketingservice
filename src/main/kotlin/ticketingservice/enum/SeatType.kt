package ticketingservice.enum

enum class SeatType {
    FIRST_CLASS,
    SECOND_CLASS
}