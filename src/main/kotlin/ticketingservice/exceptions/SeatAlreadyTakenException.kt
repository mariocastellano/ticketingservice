package ticketingservice.exceptions

class SeatAlreadyTakenException(
    override val message: String?
) : Exception(message)
