package ticketingservice.handler

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import java.io.OutputStream

class BookingHandler : HttpHandler {
    // I am now really grateful for Springboot, thanks
    override fun handle(exchange: HttpExchange) {
        when (exchange.requestMethod) {
            "POST" -> handlePost(exchange)
            else -> {
                exchange.sendResponseHeaders(405, 0)
                exchange.close()
            }
        }
    }

    private fun handlePost(exchange: HttpExchange) {
        val response = "POST /api/bookings"
        exchange.sendResponseHeaders(200, response.length.toLong())
        val outputStream: OutputStream = exchange.responseBody
        outputStream.write(response.toByteArray())
        outputStream.close()
    }
}