package ticketingservice.request

import ticketingservice.entity.Passenger
import ticketingservice.entity.Service
import ticketingservice.entity.Seat

data class BookingRequest(
    val service: Service,
    val bookingPassenger: String,
    // I am assuming each passenger has an assigned seat on the train
    val booking: List<Pair<Passenger, Seat>>
)
