package ticketingservice.request

data class TicketRequest(
    val serviceId: Int,
    val seatNumber: Int,
    val originStationId: Int,
    val destinationStationId: Int
)
