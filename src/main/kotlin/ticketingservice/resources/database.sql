
CREATE TABLE IF NOT EXISTS passengers(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS routes(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS tickets(
    id INT AUTO_INCREMENT PRIMARY KEY,
    service_id INT NOT NULL,
    seat_number VARCHAR(50) NOT NULL,
    origin_station_id INT NOT NULL,
    destination_station_id INT NOT NULL,
    passenger_id INT NOT NULL,
    FOREIGN KEY (passenger_id) REFERENCES passengers(id)
);

CREATE TABLE IF NOT EXISTS Stations(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS services(
    id INT PRIMARY KEY,
    route_id INT,
    departure_time TIMESTAMP,
    FOREIGN KEY (route_id) REFERENCES routes(id)
);

CREATE TABLE IF NOT EXISTS carriages(
    id INT PRIMARY KEY,
    service_id INT,
    capacity INT,
    class VARCHAR(50),
    FOREIGN KEY (service_id) REFERENCES services(id)
);

CREATE TABLE IF NOT EXISTS seats(
    id INT PRIMARY KEY,
    carriage_id INT,
    seat_number VARCHAR(50),
    class VARCHAR(50),
    FOREIGN KEY (carriage_id) REFERENCES carriages(id)
);