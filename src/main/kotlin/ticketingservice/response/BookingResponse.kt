package ticketingservice.response

import ticketingservice.entity.Ticket

data class BookingResponse(
    val bookingId: Int,
    val passengerName: String,
    val tickets: List<Ticket>
)
