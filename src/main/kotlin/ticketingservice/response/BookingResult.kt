package ticketingservice.response

sealed class BookingResult {
    data class Success(val bookingResponse: BookingResponse) : BookingResult()
    data class Error(val errorMessage: String) : BookingResult()
}