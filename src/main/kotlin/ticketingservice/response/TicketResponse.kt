package ticketingservice.response

data class TicketResponse(
    val ticketId: Int,
    val serviceId: Int,
    val seatNumber: String,
    val originStationId: Int,
    val destinationStationId: Int
)