package ticketingservice.respository.implementations

import ticketingservice.entity.Booking
import ticketingservice.respository.interfaces.BookingRepositoryInterface

class BookingRepositoryImpl: BookingRepositoryInterface {
    private val bookings = mutableMapOf<Int, Booking>()
    override fun createBooking(booking: Booking): Booking {
        bookings[booking.id!!] = booking
        return booking
    }
}