package ticketingservice.respository.implementations

import ticketingservice.entity.Carriage
import ticketingservice.respository.interfaces.CarriageRepositoryInterface

class CarriageRepositoryImpl: CarriageRepositoryInterface {
    private val carriages = mutableMapOf<Int, Carriage>()
    override fun createCarriage(carriage: Carriage): Carriage {
        carriages[carriage.id!!] = carriage
        return carriage
    }
    override fun updateCarriageById(carriageId: Int, carriage: Carriage): Carriage {
        carriages[carriageId] = carriage
        return carriage
    }
}