package ticketingservice.respository.implementations

import ticketingservice.entity.Passenger
import ticketingservice.respository.interfaces.PassengerRepositoryInterface

class PassengerRepositoryImpl : PassengerRepositoryInterface {
    private val passengers = mutableMapOf<Int, Passenger>()
    override fun createPassenger(passenger: Passenger): Passenger {
        passengers[passenger.id!!] = passenger
        return passenger
    }
    override fun findByName(passengerName: String): Passenger? {
        for ((_, passenger) in passengers) {
            if (passenger.name == passengerName) {
                return passenger
            }
        }
        return null
    }
}