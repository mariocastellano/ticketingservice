package ticketingservice.respository.implementations

import ticketingservice.entity.Route
import ticketingservice.respository.interfaces.RouteRepositoryInterface

class  RouteRepositoryImpl: RouteRepositoryInterface {
    private val routes = mutableMapOf<Int, Route>()
    override fun save(route: Route): Route {
        routes[route.id!!] = route
        return route
    }
}