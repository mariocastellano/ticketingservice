package ticketingservice.respository.implementations

import ticketingservice.entity.Seat
import ticketingservice.respository.interfaces.SeatRepositoryInterface

class SeatRepositoryImpl: SeatRepositoryInterface {
    private val seats = mutableMapOf<Int, Seat>()
    override fun createSeat(seat: Seat): Seat {
        seats[seat.id!!] = seat
        return seat
    }
}