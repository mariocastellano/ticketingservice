package ticketingservice.respository.implementations

import ticketingservice.entity.Seat
import ticketingservice.entity.Service
import ticketingservice.respository.interfaces.ServiceRepositoryInterface

class ServiceRepositoryImpl: ServiceRepositoryInterface {
    private val services = mutableMapOf<Int, Service>()

    override fun createService(service: Service): Service {
        services[service.id!!] = service
        return service
    }

    override fun getServiceById(serviceId: Int): Service? {
        return services[serviceId]
    }

    override fun getAvailableSeatsForCarriage(serviceId: Int): List<Seat> {
        val service = services[serviceId]
        val availableSeats = mutableListOf<Seat>()
        service?.carriages?.forEach { carriage ->
            carriage.seats?.forEach { seat ->
                if (seat.available) {
                    availableSeats.add(seat)
                }
            }
        }
        return availableSeats
    }
}