package ticketingservice.respository.implementations

import ticketingservice.entity.Station
import ticketingservice.respository.interfaces.StationRepositoryInterface

class StationRepositoryImpl: StationRepositoryInterface {
    private val stations = mutableMapOf<Int, Station>()
    override fun createStation(station: Station): Station {
        stations[station.id!!] = station
        return station
    }
}