package ticketingservice.respository.implementations

import ticketingservice.entity.Stop
import ticketingservice.respository.interfaces.StopRepositoryInterface

class StopRepositoryImpl : StopRepositoryInterface {
    private val stops = mutableMapOf<Int, Stop>()
    override fun createStop(stop: Stop): Stop {
        stops[stop.id!!] = stop
        return stop
    }
}