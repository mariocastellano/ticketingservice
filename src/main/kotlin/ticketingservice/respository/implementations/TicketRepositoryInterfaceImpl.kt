package ticketingservice.respository.implementations

import ticketingservice.entity.Ticket
import ticketingservice.respository.interfaces.TicketRepositoryInterface

class TicketRepositoryInterfaceImpl: TicketRepositoryInterface {
    private val tickets = mutableMapOf<Int, Ticket>()
    override fun save(ticket: Ticket): Ticket {
        tickets[ticket.id!!] = ticket
        return ticket
    }
    override fun findById(ticketId: Int): Ticket? {
        for ((_, ticket) in tickets) {
            if (ticket.id == ticketId) {
                return ticket
            }
        }
        return null
    }
}