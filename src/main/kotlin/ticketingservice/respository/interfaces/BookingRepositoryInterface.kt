package ticketingservice.respository.interfaces

import ticketingservice.entity.Booking

interface BookingRepositoryInterface {
    fun createBooking(booking: Booking): Booking
}