package ticketingservice.respository.interfaces

import ticketingservice.entity.Carriage

interface CarriageRepositoryInterface {
    fun createCarriage(carriage: Carriage): Carriage
    fun updateCarriageById(carriageId: Int, carriage: Carriage): Carriage
}