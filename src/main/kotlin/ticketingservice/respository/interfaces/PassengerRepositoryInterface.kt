package ticketingservice.respository.interfaces

import ticketingservice.entity.Passenger

interface PassengerRepositoryInterface {
    fun createPassenger(passenger: Passenger): Passenger
    fun findByName(passengerName: String): Passenger?
}