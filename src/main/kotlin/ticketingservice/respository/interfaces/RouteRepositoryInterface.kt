package ticketingservice.respository.interfaces

import ticketingservice.entity.Route

interface RouteRepositoryInterface {
    fun save(route: Route): Route
}