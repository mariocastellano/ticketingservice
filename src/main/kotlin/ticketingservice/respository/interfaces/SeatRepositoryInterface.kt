package ticketingservice.respository.interfaces

import ticketingservice.entity.Seat

interface SeatRepositoryInterface {
    fun createSeat(seat: Seat): Seat
}