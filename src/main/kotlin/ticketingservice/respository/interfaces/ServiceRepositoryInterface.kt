package ticketingservice.respository.interfaces

import ticketingservice.entity.Seat
import ticketingservice.entity.Service

interface ServiceRepositoryInterface {
    fun createService(service: Service): Service
    fun getServiceById(serviceId: Int): Service?
    fun getAvailableSeatsForCarriage(serviceId: Int): List<Seat>?
}