package ticketingservice.respository.interfaces

import ticketingservice.entity.Station

interface StationRepositoryInterface {
    fun createStation(station: Station): Station
}