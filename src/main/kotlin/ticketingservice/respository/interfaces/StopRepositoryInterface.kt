package ticketingservice.respository.interfaces

import ticketingservice.entity.Stop


interface StopRepositoryInterface {
    fun createStop(stop: Stop): Stop
}