package ticketingservice.respository.interfaces

import ticketingservice.entity.Ticket

interface TicketRepositoryInterface {
    fun save(ticket: Ticket): Ticket
    fun findById(ticketId: Int): Ticket?
}