package ticketingservice.service.implementation

import ticketingservice.entity.Booking
import ticketingservice.entity.Ticket
import ticketingservice.request.BookingRequest
import ticketingservice.response.BookingResponse
import ticketingservice.respository.interfaces.BookingRepositoryInterface
import ticketingservice.respository.interfaces.ServiceRepositoryInterface
import ticketingservice.exceptions.SeatAlreadyTakenException
import ticketingservice.service.interfaces.BookingServiceInterface
import ticketingservice.service.interfaces.PassengerServiceInterface

class BookingServiceImpl(
    private val passengerService: PassengerServiceInterface,
    private val ticketService: TicketServiceImpl,
    private val repositoryInterface: BookingRepositoryInterface,
    private val serviceRepository: ServiceRepositoryInterface
) : BookingServiceInterface {

    override fun isBookingAvailable(bookingRequest: BookingRequest): Boolean {
        val availableSeatsInService = serviceRepository.getAvailableSeatsForCarriage(
            bookingRequest.service.id!!
        )
        val nonAvailableSeats = bookingRequest.booking.filterNot {
            (_, seat) ->
            availableSeatsInService?.contains(seat) == true
        }.map { it.second }

        if (nonAvailableSeats.isNotEmpty()) {
            val exceptions = mutableListOf<String>()
            nonAvailableSeats.forEach { seat ->
                val passenger = bookingRequest.booking.find { it.second == seat }?.first
                val message = "Seat $seat of passenger $passenger is already taken"
                exceptions.add(message)
            }

            val errorMessage = "Can't perform booking:\n${exceptions.joinToString("\n")}"
            throw SeatAlreadyTakenException(errorMessage)
        }

        return true
    }

    override fun createBooking(bookingRequest: BookingRequest): BookingResponse {
        val bookingTickets = mutableListOf<Ticket>()
        bookingRequest.booking.forEach { (passenger, seat) ->
            val existingPassenger = passengerService.getOrCreatePassenger(passenger)
            bookingTickets.add(ticketService.createTicket(bookingRequest.service.id!!, seat.id!!, existingPassenger.id!!))
        }
        val booking = repositoryInterface.createBooking(
            Booking(
                passengerName = bookingRequest.bookingPassenger,
                tickets = bookingTickets
            )
        )
        bookingTickets.forEach { ticket ->
            ticket.bookingId = booking.id
            ticketService.updateTicketById(ticket.id!!, ticket)
        }

        return BookingResponse(booking.id!!, bookingRequest.bookingPassenger, bookingTickets.toList())
    }
}