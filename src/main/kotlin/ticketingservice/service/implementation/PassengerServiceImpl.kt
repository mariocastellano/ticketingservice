package ticketingservice.service.implementation

import ticketingservice.entity.Passenger
import ticketingservice.respository.interfaces.PassengerRepositoryInterface
import ticketingservice.service.interfaces.PassengerServiceInterface

class PassengerServiceImpl(
    private val passengerRepositoryInterface: PassengerRepositoryInterface
): PassengerServiceInterface {
    override fun createPassenger(passenger: Passenger): Passenger {
        return passengerRepositoryInterface.createPassenger(passenger)
    }

    override fun getOrCreatePassenger(passenger: Passenger): Passenger {
        return passengerRepositoryInterface.findByName(passenger.name) ?: passengerRepositoryInterface.createPassenger(passenger)
    }

    override fun getPassengerByName(passengerName: String): Passenger? {
        return passengerRepositoryInterface.findByName(passengerName)
    }
}