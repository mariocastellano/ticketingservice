package ticketingservice.service.implementation

import ticketingservice.entity.Route
import ticketingservice.service.interfaces.RouteServiceInterface

class RouteServiceImpl : RouteServiceInterface {
    override fun createRoute(route: Route): Route {
        println("Saving route to the database: ${route.name}")

        return route
    }
}