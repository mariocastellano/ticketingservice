package ticketingservice.service.implementation

import ticketingservice.entity.Seat
import ticketingservice.respository.interfaces.CarriageRepositoryInterface
import ticketingservice.respository.interfaces.SeatRepositoryInterface
import ticketingservice.service.interfaces.SeatServiceInterface

class SeatServiceImpl(
    private val seatRepositoryInterface: SeatRepositoryInterface,
    private val carriageRepositoryInterface: CarriageRepositoryInterface
) : SeatServiceInterface {
    override fun saveSeatAndUpdateCarriage(seat: Seat): Seat {
        val savedSeat = seatRepositoryInterface.createSeat(seat)

        // Update the associated carriage
        val updatedCarriage = savedSeat.carriage.copy(seats = savedSeat.carriage.seats?.plus(savedSeat))
        savedSeat.carriage.id?.let { carriageRepositoryInterface.updateCarriageById(it, updatedCarriage) }

        return savedSeat
    }
}