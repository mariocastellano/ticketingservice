package ticketingservice.service.implementation

import ticketingservice.entity.Ticket
import ticketingservice.respository.interfaces.TicketRepositoryInterface

class TicketServiceImpl(
    private val ticketRepositoryInterface: TicketRepositoryInterface
) {
    fun createTicket(ticket: Ticket): Ticket {
        return ticketRepositoryInterface.save(ticket)
    }

    fun createTicket(serviceId: Int, seatId: Int, passengerId: Int): Ticket {
        return ticketRepositoryInterface.save(
            Ticket(
                serviceId = serviceId,
                seatId = seatId,
                passengerId = passengerId
            )
        )
    }

    fun updateTicketById(ticketId: Int, ticket: Ticket): Ticket {
        return ticketRepositoryInterface.save(ticket)
    }
}
