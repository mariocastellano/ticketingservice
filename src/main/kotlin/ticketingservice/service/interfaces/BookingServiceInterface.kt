package ticketingservice.service.interfaces

import ticketingservice.request.BookingRequest
import ticketingservice.response.BookingResponse

interface BookingServiceInterface {
    fun createBooking(bookingRequest: BookingRequest): BookingResponse
    fun isBookingAvailable(bookingRequest: BookingRequest): Boolean
}