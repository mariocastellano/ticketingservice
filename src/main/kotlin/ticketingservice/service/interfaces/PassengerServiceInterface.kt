package ticketingservice.service.interfaces

import ticketingservice.entity.Passenger

interface PassengerServiceInterface {
    fun createPassenger(passenger: Passenger): Passenger
    fun getOrCreatePassenger(passenger: Passenger): Passenger
    fun getPassengerByName(passengerName: String): Passenger?
}