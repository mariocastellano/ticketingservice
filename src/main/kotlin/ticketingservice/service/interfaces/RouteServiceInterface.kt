package ticketingservice.service.interfaces

import ticketingservice.entity.Route

interface RouteServiceInterface {
    fun createRoute(route: Route): Route
}