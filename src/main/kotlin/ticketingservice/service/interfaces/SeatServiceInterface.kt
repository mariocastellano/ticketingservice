package ticketingservice.service.interfaces

import ticketingservice.entity.Seat

interface SeatServiceInterface {
    fun saveSeatAndUpdateCarriage(seat: Seat): Seat
}