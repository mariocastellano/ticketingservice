package ticketingservice.service.interfaces

import ticketingservice.entity.Ticket
import ticketingservice.response.TicketResponse

interface TicketServiceInterface {
    fun createTicket(ticket: Ticket): TicketResponse
    fun getBookingById(bookingId: Int): TicketResponse?
    fun updateTicketById(ticketId: Int, ticket: Ticket): Ticket
}