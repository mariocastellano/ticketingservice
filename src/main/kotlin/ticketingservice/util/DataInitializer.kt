package ticketingservice.util

import ticketingservice.entity.*
import ticketingservice.enum.SeatType
import ticketingservice.respository.interfaces.*
import ticketingservice.service.interfaces.SeatServiceInterface
import java.time.LocalDateTime

class DataInitializer(
    private val stationRepository: StationRepositoryInterface,
    private val routeRepositoryInterface: RouteRepositoryInterface,
    private val serviceRepositoryInterface: ServiceRepositoryInterface,
    private val carriageRepositoryInterface: CarriageRepositoryInterface,
    private val seatServiceInterface: SeatServiceInterface,
    private val passengerRepositoryInterface: PassengerRepositoryInterface,
    private val stopRepositoryInterface: StopRepositoryInterface
) {
    fun initializeData() {
        // Initialize stations
        val stations = listOf(
            "Paris", "London", "Amsterdam", "Berlin", "Enschede",
            "Hannover", "Dover", "Rotterdam", "Brussels", "Calais"
        )

        val savedStations = stations.map { stationName ->
            stationRepository.createStation(Station(name = stationName))
        }

        // Create passengers
        val passengers = listOf(
            "John", "Jack", "Joe", "James"
        )

        passengers.forEach { passengerName ->
            passengerRepositoryInterface.createPassenger(Passenger(name = passengerName))
        }

        val routes = listOf(
            Route(
                name = "Paris to London",
                stops = emptyList()
            ),
            Route(
                name = "Paris to Amsterdam",
                stops = emptyList()
            ),
            Route(
                name = "Amsterdam to Berlin",
                stops = emptyList()
            ),
            Route(
                name = "London to Amsterdam",
                stops = emptyList()
            ),
        )
        routes.forEach { route ->
            routeRepositoryInterface.save(route)
        }

        val stops = listOf(
            //Paris to London
            Stop(route = routes[0], station = savedStations[0], distanceFromPreviousStop = 0),
            Stop(route = routes[0], station = savedStations[6], distanceFromPreviousStop = 100),
            Stop(route = routes[0], station = savedStations[9], distanceFromPreviousStop = 90),
            Stop(route = routes[0], station = savedStations[1], distanceFromPreviousStop = 120),
            //Paris to Amsterdam
            Stop(route = routes[1], station = savedStations[0], distanceFromPreviousStop = 100),
            Stop(route = routes[1], station = savedStations[7], distanceFromPreviousStop = 190),
            Stop(route = routes[1], station = savedStations[8], distanceFromPreviousStop = 165),
            Stop(route = routes[1], station = savedStations[2], distanceFromPreviousStop = 80),
            //Amsterdam to Berlin
            Stop(route = routes[1], station = savedStations[2], distanceFromPreviousStop = 100),
            Stop(route = routes[1], station = savedStations[4], distanceFromPreviousStop = 200),
            Stop(route = routes[1], station = savedStations[5], distanceFromPreviousStop = 160),
            Stop(route = routes[1], station = savedStations[3], distanceFromPreviousStop = 140),
            //London to Amsterdam
            Stop(route = routes[3], station = savedStations[1], distanceFromPreviousStop = 100),
            Stop(route = routes[3], station = savedStations[9], distanceFromPreviousStop = 200),
            Stop(route = routes[3], station = savedStations[6], distanceFromPreviousStop = 160),
            Stop(route = routes[3], station = savedStations[7], distanceFromPreviousStop = 140),
            Stop(route = routes[3], station = savedStations[8], distanceFromPreviousStop = 70),
            Stop(route = routes[3], station = savedStations[2], distanceFromPreviousStop = 80),
        )

        stops.forEach { stop ->
            stopRepositoryInterface.createStop(stop)
        }

        val carriage1 = carriageRepositoryInterface.createCarriage(Carriage())
        val carriage2 = carriageRepositoryInterface.createCarriage(Carriage())
        val carriage3 = carriageRepositoryInterface.createCarriage(Carriage())
        val carriage4 = carriageRepositoryInterface.createCarriage(Carriage())

        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage1, seatNumber = "A11", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage1, seatNumber = "A12", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage1, seatNumber = "B1", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage1, seatNumber = "B2", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage2, seatNumber = "A1", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage2, seatNumber = "A2", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage2, seatNumber = "B1", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage2, seatNumber = "B2", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage3, seatNumber = "A1", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage3, seatNumber = "A2", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage3, seatNumber = "B1", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage3, seatNumber = "B2", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage4, seatNumber = "A1", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage4, seatNumber = "A2", type = SeatType.FIRST_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage4, seatNumber = "B1", type = SeatType.SECOND_CLASS))
        seatServiceInterface.saveSeatAndUpdateCarriage(Seat(carriage = carriage4, seatNumber = "B2", type = SeatType.SECOND_CLASS))

        serviceRepositoryInterface.createService(
            Service(
                route = routes[1],
                departureTime = LocalDateTime.of(2021, 4, 1, 8, 0),
                carriages = listOf(carriage1)
            )
        )
        serviceRepositoryInterface.createService(
            Service(
                route = routes[3],
                departureTime = LocalDateTime.of(2021, 4, 1, 9, 0),
                carriages = listOf(carriage2)
            )
        )
        serviceRepositoryInterface.createService(
            Service(
                route = routes[0],
                departureTime = LocalDateTime.of(2021, 4, 1, 10, 0),
                carriages = listOf(carriage3)
            )
        )
        serviceRepositoryInterface.createService(
            Service(
                route = routes[1],
                departureTime = LocalDateTime.of(2021, 4, 1, 11, 0),
                carriages = listOf(carriage4)
            )
        )
    }
}