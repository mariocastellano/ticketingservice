import mocks.mockBookingRepository
import mocks.mockPassengerService
import mocks.mockServiceRepository
import mocks.mockTicketService
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import ticketingservice.entity.*
import ticketingservice.enum.SeatType
import ticketingservice.request.BookingRequest
import ticketingservice.respository.interfaces.BookingRepositoryInterface
import ticketingservice.respository.interfaces.ServiceRepositoryInterface
import ticketingservice.service.implementation.BookingServiceImpl
import ticketingservice.service.implementation.TicketServiceImpl
import ticketingservice.service.interfaces.PassengerServiceInterface
import java.time.LocalDateTime

class BookingServiceTest {
    lateinit var bookingService: BookingServiceImpl

    @Before
    fun setUp() {

        val passengerService: PassengerServiceInterface = mockPassengerService()
        val ticketService: TicketServiceImpl = mockTicketService()
        val repositoryInterface: BookingRepositoryInterface = mockBookingRepository()
        val serviceRepositoryInterface: ServiceRepositoryInterface = mockServiceRepository()

        bookingService = BookingServiceImpl(passengerService, ticketService, repositoryInterface, serviceRepositoryInterface)
    }

    @Test
    fun testCreateBooking() {
        val passenger1 = Passenger(name = "John Doe")
        val passenger2 = Passenger(name = "Jane Doe")
        val carriage1 = Carriage()
        val carriage2 = Carriage()
        val stationParis = Station(name = "Paris")
        val stationAmsterdam = Station(name = "Amsterdam")
        val seat1 = Seat(seatNumber = "A11", carriage = carriage1, type = SeatType.FIRST_CLASS)
        val seat2 = Seat(seatNumber = "A12", carriage = carriage2, type =  SeatType.FIRST_CLASS)
        val stops = listOf(
            Stop(route = Route(name = "RouteName", stops = listOf()), station = stationParis, distanceFromPreviousStop = 0),
            Stop(route = Route(name = "RouteName", stops = listOf()), station = stationAmsterdam, distanceFromPreviousStop = 100) // Assuming distance in kilometers
        )
        val route = Route(name = "RouteName", stops = stops)
        val carriages = listOf<Carriage>()

        val service = Service(
            route = route,
            departureTime = LocalDateTime.of(2021, 4, 1, 0, 0), // April 1st, 2021
            carriages = carriages
        )
        val bookingRequest = BookingRequest(
            bookingPassenger = "John Doe",
            service = service,
            booking = listOf(passenger1 to seat1, passenger2 to seat2)
        )

        val bookingResponse = bookingService.createBooking(bookingRequest)

        assertNotNull(bookingResponse.bookingId)
        assertEquals("John Doe", bookingResponse.passengerName)
        assertEquals(2, bookingResponse.tickets.size)
    }

}
