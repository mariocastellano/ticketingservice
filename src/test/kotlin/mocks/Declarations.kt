package mocks

import org.mockito.Mockito.*
import ticketingservice.respository.interfaces.BookingRepositoryInterface
import ticketingservice.respository.interfaces.ServiceRepositoryInterface
import ticketingservice.service.implementation.TicketServiceImpl
import ticketingservice.service.interfaces.PassengerServiceInterface

fun mockPassengerService(): PassengerServiceInterface {
    return mock(PassengerServiceInterface::class.java)
}

fun mockTicketService(): TicketServiceImpl {
    return mock(TicketServiceImpl::class.java)
}

fun mockBookingRepository(): BookingRepositoryInterface {
    return mock(BookingRepositoryInterface::class.java)
}

fun mockServiceRepository(): ServiceRepositoryInterface {
    return mock(ServiceRepositoryInterface::class.java)
}